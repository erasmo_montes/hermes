all: run

clean:
	rm -rf venv && rm -rf *.egg-info && rm -rf dist && rm -rf *.log*

run:
	FLASK_APP=hermes HERMES_SETTINGS=../settings.cfg gunicorn -b 0.0.0.0 hermes:app 

test:
	HERMES_SETTINGS=../settings.cfg python -m unittest discover -s tests
