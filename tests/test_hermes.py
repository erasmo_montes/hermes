import unittest
import json
from flask_api import status

import hermes


class HermesTestCase(unittest.TestCase):

    def setUp(self):
        self.app = hermes.app.test_client()

    def test_index(self):
        rv = self.app.get('/')
        self.assertEqual({u'working': True}, rv.json)

    def test_send_message_with_get_method(self):
        body = {'to': 8292044821, 'message': 'TEST HERMES'}
        headers = {"Content-type": "Application/json"}
        response = self.app.get(
            '/api/v1/whatsaap/message/',
            data=json.dumps(body), headers=headers)
        
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
    
    def test_send_message_by_whatshap(self):
        body = {'to': 8292044821, 'message': 'TEST HERMES'}
        headers = {"Content-type": "Application/json"}
        response = self.app.post(
            '/api/v1/whatsaap/message/',
            data=json.dumps(body), headers=headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json.get('success'))
        self.assertTrue(response.json.get('custom_uid'))
    
    def test_send_message_by_whatsaap_without_to_key(self):
        headers = {"Content-type": "Application/json"}
        response = self.app.post(
            '/api/v1/whatsaap/message/',
            data=json.dumps(dict(message='PRUEBA')),headers=headers)
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_send_message_by_whatsaap_without_message_key(self):
        headers = {"Content-type": "Application/json"}
        response = self.app.post(
            '/api/v1/whatsaap/message/',
            data=json.dumps(dict(to=8292044821)),headers=headers)
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


if __name__ == '__main__':
    unittest.main()
