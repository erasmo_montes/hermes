import json
from flask import jsonify, request
from flask_api import status
from oraculo.gods.exceptions import BadRequest 

from hermes import app
from . import waboxapp


@app.route('/')
def index():
    app.logger.warning('Hermes is working')
    return jsonify({'working': True})


@app.route('/api/v1/whatsaap/message/', methods=['POST'])
def whatsaap_message():
    body = request.get_json()
    if not body.get('to'):
        message = "Not send to field"
        return dict(error=message), status.HTTP_400_BAD_REQUEST

    if not body.get('message'):
        message = "Not send field message"
        return dict(error=message), status.HTTP_400_BAD_REQUEST

    try:
        client = waboxapp.APIClient()
        return client.send_message(**body)
    except BadRequest as error:
        return dict(error=str(error)), status.HTTP_400_BAD_REQUEST
    