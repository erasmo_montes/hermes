import os
from flask_api import FlaskAPI

app = FlaskAPI(__name__)
app.config.from_object('hermes.settings')
app.config.from_envvar('HERMES_SETTINGS')

if not app.debug:
    import logging
    from logging.handlers import TimedRotatingFileHandler
    # https://docs.python.org/3.6/library/logging.handlers.html#timedrotatingfilehandler
    file_handler = TimedRotatingFileHandler(
        os.path.join(app.config['LOG_DIR'], 'hermes.log'),
        'midnight')
    file_handler.setLevel(logging.WARNING)
    file_handler.setFormatter(logging.Formatter('<%(asctime)s> <%(levelname)s> %(message)s'))
    app.logger.addHandler(file_handler)

import hermes.resources
